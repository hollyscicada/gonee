package com.kb.api.controller.notice;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class NoticeRestController {   
	  
	@Inject MasterService masterService; 
	 
	   
	/**
	 * notice 리스트 
	 */
	@PostMapping("/api/v1/notice")   
	public ResponseEntity<Map<String, Object>> cs(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String[] valids = {"page", "gubun_code"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block()); 
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param);
		
		
		int record = masterService.dataCount("mapper.NoticeMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.NoticeMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * notice 리스트 
	 */
	@PostMapping("/api/v1/notice/{notice_seq}")   
	public ResponseEntity<Map<String, Object>> read(HttpSession session, @RequestBody ProHashMap param, @PathVariable String notice_seq) throws Exception {
		param.put("notice_seq", notice_seq);
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.NoticeMapper", "read", paramMap);
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 공지사항 조회수 증가
	 */
	@PostMapping("/api/v1/notice/view")   
	public ResponseEntity<Map<String, Object>> notice_view(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"notice_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.NoticeMapper", "view_add", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
