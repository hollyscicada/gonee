package com.kb.api.controller.notice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;
import com.kb.api.util.ProHashMap;

@Controller
@RequestMapping(value = "/notice/**")
public class NoticeController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 공지사항 목록
	 * */
	@RequestMapping(value ={ "/list" }, method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pg", 0);
		map.put("page_block", 10);
		map.put("gubun_code", "M");		
		
		List<ProHashMap> notice_list = (List<ProHashMap>) masterService.dataList("mapper.NoticeMapper", "list", map);
		model.addAttribute("list", notice_list);
		
		// 광고 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MA");
		
		List<ProHashMap> ad_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("ad_banner", ad_banner);
		
		return "userLayout/notice/list";
	}
	
	/*
	 * @msg : 공지사항 상세
	 * */
	@RequestMapping(value ={ "/{seq}" }, method = RequestMethod.GET)
	public String noticeDetail(Model model, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.NoticeMapper", "read", seq));
		
		return "userLayout/notice/detail";
	} 

}
