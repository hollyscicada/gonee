package com.kb.api.controller.member;


import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/member/**")
public class MemberController {  
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 마이페이지
	 * */	
	@RequestMapping(value ={ "/mypage" }, method = RequestMethod.GET)
	public String mypage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/member/mypage";
	}
	
	/*
	 * @msg : 설정
	 * */	
	@RequestMapping(value ={ "/setting" }, method = RequestMethod.GET)
	public String setting(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/member/setting";
	}
	
}
