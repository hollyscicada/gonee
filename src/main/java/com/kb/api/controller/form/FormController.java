package com.kb.api.controller.form;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/form/**")
public class FormController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 양식 목록
	 * */
	@RequestMapping(value = {"/list"}, method = RequestMethod.GET)
	public String formList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/form/list";
	}
	
	/*
	 * @msg : 양식 상세
	 * */
	@RequestMapping(value = {"/{seq}"}, method = RequestMethod.GET)
	public String formDetail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/form/detail";
	}
	
	/*
	 * @msg : 양식 등록
	 * */
	@RequestMapping(value = {"/insert"}, method = RequestMethod.GET)
	public String formInsert(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/form/insert";
	}
	
	/*
	 * @msg : 기타 자료실
	 * */
	@RequestMapping(value = {"/form/etc"}, method = RequestMethod.GET)
	public String etcList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/form/etc";
	}

}
