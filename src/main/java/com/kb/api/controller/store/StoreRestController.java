package com.kb.api.controller.store;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.board.BoardService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class StoreRestController {   
	  
	@Inject MasterService masterService; 
	@Inject BoardService boardService; 
	 
	   
	/**
	 * store 리스트  
	 */
	@PostMapping("/api/v1/store")   
	public ResponseEntity<Map<String, Object>> store(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		 
		String[] valids = {"page"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param); 
		
		
		int record = masterService.dataCount("mapper.StoreMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.StoreMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 현장 상세 + 호수 리스트
	 */
	@PostMapping("/api/v1/store/{store_seq}")   
	public ResponseEntity<Map<String, Object>> store_read(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String store_seq) throws Exception {
		param.put("store_seq", store_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 

		HashMap paramMap = new HashMap(param); 
		HashMap<String, Object> retMap = null;
		retMap = (HashMap<String, Object>) masterService.dataRead("mapper.StoreMapper", "read", paramMap);
		
		if(retMap != null) {
			
			paramMap.put("pg", 0);  
			paramMap.put("page_block", 9999999); 
			
			List<HashMap<String, Object>> retList = (List) masterService.dataList("mapper.StoreMapper", "store_num_list", paramMap);
			retMap.put("store_num_list", retList);
		}
		
		map.put("data", retMap); 
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 호수 리스트
	 */
	@PostMapping("/api/v1/store/num/{store_seq}")   
	public ResponseEntity<Map<String, Object>> store_num_list(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String store_seq) throws Exception {
		param.put("store_seq", store_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"page"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block());  
		 

		HashMap paramMap = new HashMap(param); 
		int record = masterService.dataCount("mapper.StoreMapper", "store_num_list_cnt", paramMap); 
		List<HashMap<String, Object>> retList = (List) masterService.dataList("mapper.StoreMapper", "store_num_list", paramMap);
		
		param.setTotalCount(record); // 게시물 총 개수
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retList); 
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity)); 
		return entity; 
	}
	
	/**
	 * 호수 상세
	 */ 
	@PostMapping("/api/v1/store/num/read")   
	public ResponseEntity<Map<String, Object>> store_num_read(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String[] valids = {"store_seq","numbers"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		 

		HashMap paramMap = new HashMap(param); 
		HashMap<String, Object> retMap = null;
		retMap = (HashMap<String, Object>) masterService.dataRead("mapper.StoreMapper", "store_num_read", paramMap);
		map.put("data", retMap); 
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	
	/**
	 * 직급별 수수료 계산기 내역 저장
	 */ 
	@PostMapping("/api/v1/store/calculator/add")     
	public ResponseEntity<Map<String, Object>> store_calculator_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"store_seq", "numbers"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataCreate("mapper.StoreMapper", "store_calculator_add", paramMap);
		
		if(record > 0) {
			record += masterService.dataCreate("mapper.StoreMapper", "store_calculator_ratio_add", paramMap);
			
			if(record > 1) {
				map.put("success", true);
			}
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 직급별 수수료 계산기 저장 목록 + 차수리스트
	 */
	@PostMapping("/api/v1/store/calculator")   
	public ResponseEntity<Map<String, Object>> store_calculator(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 

		HashMap paramMap = new HashMap(param); 
		List<HashMap<String, Object>> retList = null;
		retList = (List) masterService.dataList("mapper.StoreMapper", "store_calculator", paramMap);
		
		if(retList != null && retList.size() > 0) {
			
			for(HashMap<String, Object> retMap : retList) {
				List<HashMap<String, Object>> retSubList = (List) masterService.dataList("mapper.StoreMapper", "store_calculator_ratio", retMap);
				retMap.put("ratio_list", retSubList);
			}
		}
		
		map.put("data", retList); 
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
