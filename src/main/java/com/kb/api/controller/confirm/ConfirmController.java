package com.kb.api.controller.confirm;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
@RequestMapping(value = "/confirm/**")
public class ConfirmController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 제출 서류 목록
	 * */
	@RequestMapping(value = {"/list"}, method = RequestMethod.GET)
	public String formList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/confirm/list";
	}
	
	/*
	 * @msg : 제출 서류 상세
	 * */
	@RequestMapping(value = {"/{seq}"}, method = RequestMethod.GET)
	public String formDetail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/confirm/detail";
	}
	
	/*
	 * @msg : 제출 서류 등록
	 * */
	@RequestMapping(value = {"/insert/{seq}"}, method = RequestMethod.GET)
	public String submissionDocument(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String seq) throws Exception {
		model.addAttribute("board_seq", seq);
		return "userLayout/confirm/insert";
	}

}
