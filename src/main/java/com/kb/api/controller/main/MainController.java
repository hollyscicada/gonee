package com.kb.api.controller.main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kb.api.service.MasterService;
import com.kb.api.util.ProHashMap; 

@Controller
public class MainController {  
	 
	@Inject MasterService masterService;  
	@GetMapping("/main") 
	public String main(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pg", 0);
		map.put("page_block", 10);
		map.put("gubun_code", "M");
		
		List<ProHashMap> notice_list = (List<ProHashMap>) masterService.dataList("mapper.NoticeMapper", "list", map);
		model.addAttribute("notice_list", notice_list);
		
		// 구인 list
		map = new HashMap<String, Object>();
		map.put("up_code", "BOARD");
		map.put("type_code", "JOB");
		map.put("pg", 0);
		map.put("page_block", 10);
		
		List<ProHashMap> list = (List<ProHashMap>) masterService.dataList("mapper.BoardMapper", "list", map);
		model.addAttribute("list", list);
		
		// 메인 상단 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MT");
		
		List<ProHashMap> top_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("top_banner", top_banner);
		
		// 광고 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MA");
		
		List<ProHashMap> ad_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("ad_banner", ad_banner);
		
		// 지역 list		
		List<ProHashMap> zone_list = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "zone_list", null);
		model.addAttribute("zone_list", zone_list);
		
		// 자동 로그인 쿠키 생성 
		if(session.getAttribute("auto_login") == "true") {
			Cookie loginCookie = new Cookie("loginCookie", session.getId());
			loginCookie.setMaxAge(60*60*24*7);
			
			response.addCookie(loginCookie);
		}
		
		return "userLayout/Main";
	}
}
