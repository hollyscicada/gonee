package com.kb.api.controller.board;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@Controller
public class BoardController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 전단지 / 브로슈어
	 * */
	@RequestMapping(value = {"/brochure/list"}, method = RequestMethod.GET)
	public String brochureList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> retMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pg", 0);
		map.put("page_block", 99);
		map.put("up_code", "BOARD");		
		map.put("type_code", "BRO");		
		map.put("member_seq", retMap.get("member_seq"));
		
		List<ProHashMap> list = (List<ProHashMap>) masterService.dataList("mapper.BoardMapper", "list", map);
		model.addAttribute("list", list);
		
		// 광고 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MA");
		
		List<ProHashMap> ad_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("ad_banner", ad_banner);
		
		return "userLayout/brochure/list";
	}
	
	/*
	 * @msg : 전단지 / 브로슈어 상세
	 * */
	@RequestMapping(value = {"/brochure/{seq}"}, method = RequestMethod.GET)
	public String brochureDetail(Model model, HttpSession session, @PathVariable String seq) throws Exception {
		HashMap<String, Object> retMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("board_seq", seq);
		map.put("member_seq", retMap.get("member_seq"));
		
		model.addAttribute("detail", masterService.dataRead("mapper.BoardMapper", "read", map));
		return "userLayout/brochure/detail";
	}
	
	/*
	 * @msg : 전단지 / 브로슈어 등록
	 * */
	@RequestMapping(value = {"/brochure/insert"}, method = RequestMethod.GET)
	public String brochureInsert(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/brochure/insert";
	}
	
	/*
	 * @msg : 구인 목록
	 * */
	@RequestMapping(value = {"/jobOffer/list"}, method = RequestMethod.GET)
	public String jobOfferList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> retMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pg", 0);
		map.put("page_block", 99);
		map.put("up_code", "BOARD");		
		map.put("type_code", "JOB");		
		map.put("member_seq", retMap.get("member_seq"));
		
		List<ProHashMap> list = (List<ProHashMap>) masterService.dataList("mapper.BoardMapper", "list", map);
		model.addAttribute("list", list);
		
		// 광고 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MA");
		
		List<ProHashMap> ad_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("ad_banner", ad_banner);
		
		return "userLayout/jobOffer/list";
	}
	
	/*
	 * @msg : 구인 상세
	 * */
	@RequestMapping(value = {"/jobOffer/{seq}"}, method = RequestMethod.GET)
	public String jobOfferDetail(Model model, HttpSession session, @PathVariable String seq) throws Exception {
		HashMap<String, Object> retMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("board_seq", seq);
		map.put("member_seq", retMap.get("member_seq"));
		
		model.addAttribute("detail", masterService.dataRead("mapper.BoardMapper", "read", map));
		model.addAttribute("board_seq", seq);
		
		return "userLayout/jobOffer/detail";
	}
	
	/*
	 * @msg : 구인 수정
	 * */
	@RequestMapping(value = {"/jobOffer/update/{seq}"}, method = RequestMethod.GET)
	public String jobOfferUpdate(Model model, HttpSession session, @PathVariable String seq) throws Exception {
		model.addAttribute("update", masterService.dataRead("mapper.BoardMapper", "read", seq));
		
		return "userLayout/jobOffer/update";
	}
	
	/*
	 * @msg : 구인 등록
	 * */
	@RequestMapping(value = {"/jobOffer/insert"}, method = RequestMethod.GET)
	public String jobOfferInsert(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/jobOffer/insert";
	}
	
	/*
	 * @msg : 분양 목록
	 * */
	@RequestMapping(value = {"/sale/list"}, method = RequestMethod.GET)
	public String saleList(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> retMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pg", 0);
		map.put("page_block", 99);
		map.put("up_code", "BOARD");		
		map.put("type_code", "SEL");		
		map.put("member_seq", retMap.get("member_seq"));
		
		List<ProHashMap> list = (List<ProHashMap>) masterService.dataList("mapper.BoardMapper", "list", map);
		model.addAttribute("list", list);
		
		// 광고 배너
		map = new HashMap<String, Object>();
		map.put("location_code", "MA");
		
		List<ProHashMap> ad_banner = (List<ProHashMap>) masterService.dataList("mapper.CommonMapper", "main_img_list", map);
		model.addAttribute("ad_banner", ad_banner);
		
		return "userLayout/sale/list";
	}
	
	/*
	 * @msg : 분양 지도
	 * */
	@RequestMapping(value = {"/sale/map"}, method = RequestMethod.GET)
	public String saleMap(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/sale/map";
	}
	
	/*
	 * @msg : 분양 수정
	 * */
	@RequestMapping(value = {"/sale/update/{seq}"}, method = RequestMethod.GET)
	public String saleUpdate(Model model, HttpSession session, @PathVariable String seq) throws Exception {
		model.addAttribute("update", masterService.dataRead("mapper.BoardMapper", "read", seq));
		return "userLayout/sale/update";
	}
	
	/*
	 * @msg : 분양 상세
	 * */
	@RequestMapping(value = {"/sale/{seq}"}, method = RequestMethod.GET)
	public String saleDetail(Model model, HttpSession session, @PathVariable String seq) throws Exception {
		model.addAttribute("detail", masterService.dataRead("mapper.BoardMapper", "read", seq));
		
		return "userLayout/sale/detail";
	}
	
	/*
	 * @msg : 분양 등록
	 * */
	@RequestMapping(value = {"/sale/insert"}, method = RequestMethod.GET)
	public String sale(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/sale/insert";
	}
}
