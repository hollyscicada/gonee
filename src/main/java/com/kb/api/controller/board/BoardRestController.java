package com.kb.api.controller.board;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.board.BoardService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class BoardRestController {   
	  
	@Inject MasterService masterService; 
	@Inject BoardService boardService; 
	
	 
	   
	/**
	 * board 리스트 
	 */
	@PostMapping("/api/v1/board")   
	public ResponseEntity<Map<String, Object>> cs(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		String authToken = request.getHeader(Common.AUTHORIZATIONS);
		if(!StringUtils.isEmpty(authToken)) {
			String member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		
		
		 
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String[] valids = {"page", "up_code", "type_code"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		
		if(!StringUtils.isEmpty(param.get("search_type")) && param.get("search_type").equals("member")) {
			param.put("search_value", param.get("member_seq"));
		}
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param); 
		
		
		int record = masterService.dataCount("mapper.BoardMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.BoardMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * board_comment_member 내가작성한 댓글
	 */
	@PostMapping("/api/v1/board/comment/member")   
	public ResponseEntity<Map<String, Object>> board_comment_member(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
	
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"page"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 
		
		HashMap paramMap = new HashMap(param); 
		
		
		int record = masterService.dataCount("mapper.BoardMapper", "board_comment_member_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.BoardMapper", "board_comment_member", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	 
	/** 
	 * board 상세 
	 */ 
	@PostMapping("/api/v1/board/{board_seq}")   
	public ResponseEntity<Map<String, Object>> cs(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		
		String authToken = request.getHeader(Common.AUTHORIZATIONS);
		if(!StringUtils.isEmpty(authToken)) {
			String member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		param.put("board_seq", board_seq);
		 
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		HashMap paramMap = new HashMap(param); 
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.BoardMapper", "read", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/** 
	 * 게시글 삭제
	 */ 
	@PostMapping("/api/v1/board/del/{board_seq}")     
	public ResponseEntity<Map<String, Object>> notice_view(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_del", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	   
	/**  
	 * 게시글 등록 
	 */ 
	@PostMapping("/api/v1/board/add")     
	public ResponseEntity<Map<String, Object>> board_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"up_code", "type_code", "title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		
		try {
			map = boardService.board_add(paramMap, map);
		}catch(Exception e) {
			map.put("success", false); 
			map.put("errorMsg", "게시글 등록작업 중 에러가 발생하였습니다.");
		}
		 
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/** 
	 * 게시글 수정
	 */ 
	@PostMapping("/api/v1/board/update/{board_seq}")     
	public ResponseEntity<Map<String, Object>> notice_update(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		
		String[] valids = {"title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_update", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	 
	/** 
	 * 게시글 신고
	 */  
	@PostMapping("/api/v1/board/sue/add")     
	public ResponseEntity<Map<String, Object>> notice_sue_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"board_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false); 
		 
		HashMap paramMap = new HashMap(param);
		map = boardService.notice_sue_add(paramMap, map);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	
	/** 
	 * 댓글 신고
	 */ 
	@PostMapping("/api/v1/comment/sue/add")     
	public ResponseEntity<Map<String, Object>> comment_sue_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"comment_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		map = boardService.comment_sue_add(paramMap, map);
		
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}  
	
	
	/**
	 * 조회수 증가
	 */
	@PostMapping("/api/v1/board/view")   
	public ResponseEntity<Map<String, Object>> board_view(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"board_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "view_add", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 추천수 증가
	 */
	@PostMapping("/api/v1/board/good")   
	public ResponseEntity<Map<String, Object>> board_good(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"board_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "good_add", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 구인게시판 서류제출 
	 */
	@PostMapping("/api/v1/board/paper")    
	public ResponseEntity<Map<String, Object>> board_paper(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String[] valids = {"board_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataCreate("mapper.BoardMapper", "board_paper_add", paramMap);
		
		if(record > 0) {
			masterService.dataCreate("mapper.BoardMapper", "board_paper_attach_add", paramMap);
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 구인게시판 내가 제출한 서류 목록
	 */
	@PostMapping("/api/v1/board/paper/member")    
	public ResponseEntity<Map<String, Object>> board_paper_member(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		
		 
		List<HashMap<String, Object>> retList = null;
		retList = (List<HashMap<String, Object>>) masterService.dataList("mapper.BoardMapper", "board_paper_member", paramMap);
		
		if(retList != null && retList.size() > 0) {
			for(int i = 0 ; i < retList.size() ; i++) {
				List<HashMap<String, Object>> retSubList = (List<HashMap<String, Object>>) masterService.dataList("mapper.BoardMapper", "board_paper_member_attach", retList.get(i));
				retList.get(i).put("attach_list", retSubList);
			}
		}
		map.put("data", retList);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 구인게시판 내가 제출한 서류 상세
	 */
	@PostMapping("/api/v1/board/paper/member/{paper_seq}")    
	public ResponseEntity<Map<String, Object>> board_paper_member_read(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String paper_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("paper_seq", paper_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		
		 
		HashMap<String, Object> retMap = null;
		retMap = (HashMap<String, Object>) masterService.dataRead("mapper.BoardMapper", "board_paper_member_read", paramMap);
		
		if(retMap != null) {
				List<HashMap<String, Object>> retSubList = (List<HashMap<String, Object>>) masterService.dataList("mapper.BoardMapper", "board_paper_member_attach", retMap);
				retMap.put("attach_list", retSubList);
		}
		map.put("data", retMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 제출한 서류 삭제 다중
	 */
	@PostMapping("/api/v1/board/paper/del")    
	public ResponseEntity<Map<String, Object>> board_paper_del(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"paper_seqs"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		if(!StringUtils.isEmpty(param.get("paper_seqs"))) {
			List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) param.get("paper_seqs");
			int record = 0;
			for(int i = 0 ; i < list.size() ; i++) {
				list.get(i).put("member_seq", member_seq);
				record += masterService.dataUpdate("mapper.BoardMapper", "board_paper_del", list.get(i));
			}
			if(record > 0) {
				map.put("success", true);
			}
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 댓글추가
	 */
	@PostMapping("/api/v1/board/comment/add")    
	public ResponseEntity<Map<String, Object>> board_comment(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"board_seq", "content", "depth"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataCreate("mapper.BoardMapper", "board_comment_add", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 댓글 리스트 
	 */ 
	@PostMapping("/api/v1/board/comment/{board_seq}")    
	public ResponseEntity<Map<String, Object>> comment_list(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String authToken = request.getHeader(Common.AUTHORIZATIONS);
		if(!StringUtils.isEmpty(authToken)) {
			String member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		param.put("board_seq", board_seq);
		  
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		
		String[] valids = {"depth"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		 
		HashMap paramMap = new HashMap(param); 
		List<HashMap<String, Object>> retMapList = (List<HashMap<String, Object>>) masterService.dataList("mapper.BoardMapper", "board_comment_list", paramMap);
		
		for(int i = 0 ; i < retMapList.size() ; i++) {
			
			
			retMapList.get(i).put("member_seq", paramMap.get("member_seq"));
			retMapList.get(i).put("depth", "2");
			retMapList.get(i).put("sort_name", "create_dt");
			retMapList.get(i).put("sort_value", "asc");
			retMapList.get(i).put("parent_seq", retMapList.get(i).get("comment_seq"));
			
			
			List<HashMap<String, Object>> retSubMapList = (List<HashMap<String, Object>>) masterService.dataList("mapper.BoardMapper", "board_comment_list", retMapList.get(i));
			retMapList.get(i).put("sub_comment_list", retSubMapList);
		}
		map.put("data", retMapList);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	
	/**
	 * 댓글 화면에 마스킹여부
	 */
	@PostMapping("/api/v1/board/comment/view")    
	public ResponseEntity<Map<String, Object>> comment_view_update(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"comment_seq", "view_yn"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataCreate("mapper.BoardMapper", "comment_view_update", paramMap);
		
		if(record > 0) { 
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**  
	 * 게시글 등록 (분양가)
	 */ 
	@PostMapping("/api/v1/board/sel/add")     
	public ResponseEntity<Map<String, Object>> board_sel_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		 
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String[] valids = {"up_code", "type_code", "title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false); 
		 
		HashMap paramMap = new HashMap(param);
		
		 
		
		try {
			map = boardService.board_sel_add(paramMap, map);
		}catch(Exception e) {
			map.put("success", false); 
			map.put("errorMsg", "게시글 등록작업 중 에러가 발생하였습니다.");
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	
	
	
	/** 
	 * 게시글 수정 (분양가)
	 */ 
	@PostMapping("/api/v1/board/update/sel/{board_seq}")     
	public ResponseEntity<Map<String, Object>> board_update_sel(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		
		String[] valids = {"title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_update", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	
	/**  
	 * 게시글 등록 (브로셔)
	 */ 
	@PostMapping("/api/v1/board/bro/add")     
	public ResponseEntity<Map<String, Object>> board_bro_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		 
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"up_code", "type_code", "title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false); 
		 
		HashMap paramMap = new HashMap(param);
		try {
			map = boardService.board_bro_add(paramMap, map);
		}catch(Exception e) {
			map.put("success", false); 
			map.put("errorMsg", "게시글 등록작업 중 에러가 발생하였습니다.");
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	 
	
	/** 
	 * 게시글 수정 (브로셔)
	 */ 
	@PostMapping("/api/v1/board/update/bro/{board_seq}")     
	public ResponseEntity<Map<String, Object>> board_update_bro(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		String[] valids = {"title", "content", "img_file", "addr"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_update", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	
}
