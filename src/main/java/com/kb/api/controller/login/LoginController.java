package com.kb.api.controller.login;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
public class LoginController {  
	
	@Inject MasterService masterService;
	
	/**
	 * @msg : 로그인
	 */
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		//authToken
		HashMap<String, Object> member = (HashMap<String, Object>) session.getAttribute("MEMBER");
				
		if(member != null) {
			System.out.println("login");
			return "forward:/main";
		} else {
			System.out.println("non login");
			return "userLayoutNotLogin/Login";
		}
	}
	
	/**
	 * @msg : 로그아웃
	 */
	@RequestMapping(value = {"/logout"}, method =  { RequestMethod.GET , RequestMethod.POST })
	public String logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		return "forward:/";
	}
}
