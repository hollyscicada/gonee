package com.kb.api.controller.login;
 
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class LoginRestController {
	
	@Inject MasterService masterService;
	   
	/**
	 * 로그인 & 회원가입 
	 */
	@PostMapping("/api/v1/login")  
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"phone"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "login", paramMap);
     		
 
		int record = 0;
		if (retMap == null) {
			record = masterService.dataCreate("mapper.LoginMapper", "sign", paramMap);
			map.put("type", "SIGN");
		}else {
			record = 1;
			map.put("type", "LOGIN");
		}
		
		if(record > 0) {
			retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_id", paramMap);
			retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
			retMap.put("custom_pw", retMap.get("custom_pw"));
			retMap.put("type", map.get("type"));
			session.setAttribute("MEMBER", retMap);
			
			map.put("authToken", retMap.get("authToken"));
			map.put("custom_pw", retMap.get("custom_pw"));
			map.put("success", true);
			
			
			if(!StringUtils.isEmpty(paramMap.get("push_token"))) {
				retMap.put("push_token", paramMap.get("push_token"));
				masterService.dataUpdate("mapper.LoginMapper", "push_token_update", retMap);
			}
			
			
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 자동로그인
	 */
	@PostMapping("/api/v1/login/auto")  
	public ResponseEntity<Map<String, Object>> auto_login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"custom_pw"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_auto", paramMap);
		
		
		if(retMap != null) {
			retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", retMap);
			retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
			retMap.put("custom_pw", retMap.get("custom_pw"));
			session.setAttribute("MEMBER", retMap);
			
			map.put("authToken", retMap.get("authToken"));
			map.put("custom_pw", retMap.get("custom_pw"));
			map.put("success", true);
			
			if(!StringUtils.isEmpty(paramMap.get("push_token"))) {
				retMap.put("push_token", paramMap.get("push_token"));
				masterService.dataUpdate("mapper.LoginMapper", "push_token_update", retMap);
			}
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	
	
	/**
	 * 사용자 정보
	 */
	@PostMapping("/api/v1/user/info/seq")  
	public ResponseEntity<Map<String, Object>> user_info(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap); 
		map.put("data", retMap);
 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 아이디 찾기
	 */
	@PostMapping("/api/v1/find/id")  
	public ResponseEntity<Map<String, Object>> find_id(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "find_id", paramMap);
		
		if(retMap != null) {
			map.put("success", true);
			map.put("data", retMap);
			
			//전화번호 sms로 이메일을 전송한다.
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 공통 중복체크 ( 메일, 아이디)
	 */
	@PostMapping("/api/v1/common/chk")  
	public ResponseEntity<Map<String, Object>> common_chk(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"column_name","column_value"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "common_chk", paramMap);
		
		if(retMap != null) {
			map.put("success", true);//중복되는게 잇을경우 true
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	
	/**
	 * 닉네임변경
	 */
	@PostMapping("/api/v1/change/info")   
	public ResponseEntity<Map<String, Object>> change_info(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		int record = masterService.dataUpdate("mapper.LoginMapper", "change_info", paramMap);
		if(record > 0) {
			HashMap<String, Object> retMap = null;
			retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap);
			session.setAttribute("MEMBER", retMap);
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 내정보중 수신정보 변경
	 */ 
	@PostMapping("/api/v1/change/push")  
	public ResponseEntity<Map<String, Object>> change_push(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		int record = masterService.dataUpdate("mapper.LoginMapper", "change_push", paramMap);
		
		if(record > 0) {
			HashMap<String, Object> retMap = null;
			retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap);
			retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
			retMap.put("custom_pw", retMap.get("custom_pw"));
			
			session.setAttribute("MEMBER", retMap);
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 회원탈퇴
	 */ 
	@PostMapping("/api/v1/member/delete")  
	public ResponseEntity<Map<String, Object>> member_delete(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param); 
		int record = masterService.dataUpdate("mapper.LoginMapper", "member_delete", paramMap);
		
		if(record > 0) {
			session.invalidate();
			map.put("success", true); 
		} 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	/**
	 * 회원탈퇴 소셜가입자
	 */ 
	@PostMapping("/api/v1/member/delete/sns")  
	public ResponseEntity<Map<String, Object>> member_delete_sns(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param); 
		int record = masterService.dataUpdate("mapper.LoginMapper", "member_delete_sns", paramMap);
		
		if(record > 0) {
			session.invalidate();
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
}
