package com.kb.api.controller.estimate;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
public class EstimateController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 견적서
	 * */
	@RequestMapping(value = {"/estimate"}, method = RequestMethod.GET)
	public String estimate(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/estimate";
	}
	
}
