package com.kb.api.controller.calculator;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;

@Controller
public class CalculatorController {
	
	@Inject MasterService masterService;
	
	/*
	 * @msg : 계산기
	 * */
	@RequestMapping(value = {"/calc"}, method = RequestMethod.GET)
	public String calculator(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		return "userLayout/calculator";
	}

}
