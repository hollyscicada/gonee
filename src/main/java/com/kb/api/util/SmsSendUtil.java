package com.kb.api.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.kb.api.model.SmsModel;

@Component
public class SmsSendUtil {
	
	
	@Value("#{props['sms.key']}")
	public String sms_key; 

	@Value("#{props['sms.id']}")
	public String sms_id; 
	
	@Value("#{props['sms.sender']}")
	String sms_sender;
	
	public SmsModel smsSend(Map<String, String> dataMap) {
		try{
			
			/**************** 문자전송하기 예제 ******************/
			/* "result_code":결과코드,"message":결과문구, */
			/* "msg_id":메세지ID,"error_cnt":에러갯수,"success_cnt":성공갯수 */
			/* 동일내용 > 전송용 입니다.  
			/******************** 인증정보 ********************/
			HashMap<String, String> sms = new HashMap<String, String>();
			
			sms.put("user_id", sms_id); // SMS 아이디
			sms.put("key", sms_key); //인증키
			 
			/******************** 인증정보 ********************/
			
			/******************** 전송정보 ********************/
//			sms.put("destination", "01111111111|담당자,01111111112|홍길동"); // 수신인 %고객명% 치환
			sms.put("sender", sms_sender); // 발신번호

			
			sms.put("receiver", dataMap.get("TARGET_ADDRESS")); // 수신번호
			sms.put("title", dataMap.get("TITLE")); //  LMS, MMS 제목 (미입력시 본문중 44Byte 또는 엔터 구분자 첫라인)
			sms.put("msg", 	dataMap.get("CONTENTS"));
			
			
			
			
			
			  URL url = new URL("https://apis.aligo.in/send/");
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("POST");
				con.setDoOutput(true);
				con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");	
			
			
			
			String queryString = getDataString(sms);
			byte[] postData = queryString.getBytes( StandardCharsets.UTF_8 );
			 DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	            wr.write(postData);
	            wr.flush();
	            wr.close();
			
	            
	            
	            
            int responseCode = con.getResponseCode();
    		BufferedReader br;
    		if (responseCode == 200) {
    			br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
    		} else {
    			br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "utf-8"));
    		}
    		String inputLine;
    		StringBuffer response = new StringBuffer();
    		while ((inputLine = br.readLine()) != null) {
    			response.append(inputLine);
    		}
    		br.close();
    		
    		
    		SmsModel smsModel = Util.convertJsonToObject(response.toString(), SmsModel.class);
    		System.out.println(response.toString());
    		return smsModel;
			
		}catch(Exception e){
			return null;
		}
	}
	
	private String getDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;
	    for(Map.Entry<String, String> entry : params.entrySet()){
	        if (first)
	            first = false;
	        else
	            result.append("&");    
	        result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
	        result.append("=");
	        result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
	    }    
	    return result.toString();
	}
	
}
