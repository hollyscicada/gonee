package com.kb.api.service.fileupload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.board.BoardService;
import com.kb.api.util.Common;
import com.kb.api.util.Util;


@Service
public class FileUploadService {
	@Inject MasterDao MasterDao;
	@Inject BoardService boardService;

	
	/**
	 * @param realPath : 해당 경로에 디렉토리가 있다면 넘어가고 없다면 디렉토리를 생성한다.
	 */
	public static void mkDir(String realPath){
		File updir = new File(realPath);
		if (!updir.exists()) updir.mkdirs();
	}
	
	/**
	 * 엑셀파일 업로드
	 * @throws Exception 
	 */
	@Transactional("transactionManagerTran")
	public HashMap<String, Object> multipart_upload2(HttpServletRequest request, String realPath, String webUrl) throws Exception{
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("member_seq", member_seq);
		map.put("addr", request.getParameter("addr"));
		map.put("is_force", Boolean.valueOf(request.getParameter("is_force")));
		
		
		Map<String, Object> kakoRstMap = new HashMap<String, Object>();
		kakoRstMap = boardService.getKakaoLocation(map, kakoRstMap);
		
		
		if(!Boolean.valueOf(kakoRstMap.get("kakao_success").toString())) {
			HashMap<String, Object> rst = new HashMap<String, Object>();
			rst.put("success", false);
			if(!StringUtils.isEmpty(kakoRstMap.get("errorMsg"))) {
				rst.put("errorMsg", kakoRstMap.get("errorMsg"));
			}
			return rst;
		}else {
			map.put("x", kakoRstMap.get("x"));
			map.put("y", kakoRstMap.get("y"));
		}
		
		
		mkDir(realPath);
		String filename = "";
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		MultipartFile mrequest = null;
		
		
		while(iterator.hasNext()){
			mrequest = multipartHttpServletRequest.getFile(iterator.next());
			
			String filenameTemp = mrequest.getOriginalFilename();
			String ext = filenameTemp.substring(filenameTemp.lastIndexOf(".") + 1); //확장자 구하기
			filename += UUID.randomUUID().toString().replace("-", "")+"."+ext;
			if(mrequest.isEmpty() == false) {
				mrequest.transferTo(new File(realPath + filename));
				 
				/*엑셀 파싱 함수호출*/
				map = excelParsing(filename, webUrl, realPath, map);
				
			}
		}
		return map;
	}
	
	/**
	 * 엑셀 파싱
	 */
	public HashMap<String, Object> excelParsing(String filename, String webUrl, String realPath, HashMap<String, Object> map) throws IOException {
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("success", false);
		
		String store_name = getSheetTwoData(realPath + "/" + filename);
		HashMap<String, Object> store_read = null;
		store_read = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.FileMapper", "find_store_name", store_name);
		
		boolean is_force = Boolean.valueOf(map.get("is_force").toString());
		if(store_read != null) {
			if(is_force) {
				
				HashMap paramMap = new HashMap();
				paramMap.put("store_seq", store_read.get("store_seq"));
				
				List<HashMap<String, Object>> list = getSheetOneData(realPath + "/" + filename);
				paramMap.put("list", list);
				
				/**
				 * 기존 호수 전체삭제 / 업데이트를 하면 리소스 낭비라고 판단하에
				 */
				int record = MasterDao.dataDeleteTra("mapper.FileMapper", "store_num_del", paramMap);
				if(record > 0) {
					
					/**
					 * 호수 등록
					 */
					record += MasterDao.dataCreateTra("mapper.FileMapper", "store_num_add", paramMap);
					if(record > 1) {
						rstMap.put("success", true);
					}
				}
				
				rstMap.put("success", true);
			}else {
				rstMap.put("errorMsg", "이전에 등록되어 있는 현장입니다.");
				rstMap.put("errorCode", "501");
			}
		}else {
			
			 
			/**
			 * 엑셀등록
			 */
			HashMap paramMap = new HashMap(); 
			paramMap.put("member_seq", map.get("member_seq"));
			paramMap.put("file_name", filename);
			paramMap.put("file_url", webUrl+filename);
			paramMap.put("name", store_name);
			paramMap.put("addr", map.get("addr"));
			paramMap.put("x", map.get("x"));
			paramMap.put("y", map.get("y"));
			int record = MasterDao.dataCreateTra("mapper.FileMapper", "excel_add", paramMap);
			
			if(record == 1) {
				
				/**
				 * 스토어 등록
				 */
				record += MasterDao.dataCreateTra("mapper.FileMapper", "store_add", paramMap);
				
				if(record == 2) {
					
					List<HashMap<String, Object>> list = getSheetOneData(realPath + "/" + filename);
					paramMap.put("list", list);
					
					/**
					 * 호수 등록
					 */
					record += MasterDao.dataCreateTra("mapper.FileMapper", "store_num_add", paramMap);
					
					if(record > 2) {
						rstMap.put("success", true);
					}
				}
			}
		}
		
		return rstMap;
	}
	
	
	
	/**
	 * 시트 1번에서 호수리스트를 가져온다
	 */
	public List<HashMap<String, Object>> getSheetOneData(String winFilePath) throws IOException {
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		
		FileInputStream fis=new FileInputStream(winFilePath);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		int rowindex=0;
		int columnindex=0;
		
		//시트 수 (첫번째에만 존재하므로 0을 준다)
		//만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
		XSSFSheet sheet=workbook.getSheetAt(0);
		
		//행의 수
		int rows=sheet.getPhysicalNumberOfRows();
//		System.out.println("---------------------------------------------------------------------------------------------------------");
		for(rowindex=1;rowindex<rows;rowindex++){
			
			if(rowindex >= 6) {
				//행을읽는다
				XSSFRow row=sheet.getRow(rowindex);
			    if(row !=null){
			    	
			    	HashMap<String, Object> rstMap = new HashMap<String, Object>();
			    	
			        //셀의 수
			        int cells=row.getPhysicalNumberOfCells();
			        for(columnindex=0;columnindex<=cells;columnindex++){
			        	
			        	if(!StringUtils.isEmpty(rstMap.get("numbers")) || columnindex < 2){
				            //셀값을 읽는다
				            XSSFCell cell=row.getCell(columnindex);
				            String value="";
				            //셀이 빈값일경우를 위한 널체크
				            if(cell==null){
				                continue;
				            }else{
				                //타입별로 내용 읽기
				                switch (cell.getCellType()){
				                case XSSFCell.CELL_TYPE_FORMULA:
				                    value=cell.getCellFormula();
				                    break;
				                case XSSFCell.CELL_TYPE_NUMERIC:
				                    value=cell.getNumericCellValue()+"";
				                    NumberFormat nf = NumberFormat.getInstance(); 
				                    value= nf.format(Double.parseDouble(value)).replaceAll(",", "");
				                    break;
				                case XSSFCell.CELL_TYPE_STRING:
				                    value=cell.getStringCellValue()+"";
				                    break;
				                case XSSFCell.CELL_TYPE_BLANK:
				                    value=cell.getBooleanCellValue()+"";
				                    break;
				                case XSSFCell.CELL_TYPE_ERROR:
				                    value=cell.getErrorCellValue()+"";
				                    break;
				                }
				            }
				            
				            
	//			            System.out.print(" | ");
	//			            System.out.print(value);
				            switch(columnindex) {
				            	case 1 : 
				            		rstMap.put("numbers", Math.round(Double.parseDouble(value)));
				            		break;
				            	case 3 : 
				            		rstMap.put("exclusive_area", cell.getNumericCellValue()); //소수점은 해당 cell 타입으로
				            		break;
				            	case 4 : 
				            		rstMap.put("exclusive_area2", cell.getNumericCellValue()); //소수점은 해당 cell 타입으로
				            		break;
				            	case 5 : 
				            		double exclusive_area = Double.parseDouble(rstMap.get("exclusive_area").toString());
				            		double exclusive_area2 = Double.parseDouble(rstMap.get("exclusive_area2").toString());
				            		double contact_area_cal = exclusive_area+exclusive_area2;
				            		double contact_area = Math.round(contact_area_cal*100)/100.0;
				            		rstMap.put("contact_area_cal", contact_area_cal);
				            		rstMap.put("contact_area", contact_area);
				            		break;
				            	case 7 : 
				            		rstMap.put("ground_pay", value);
				            		break;
				            	case 8 : 
				            		rstMap.put("make_pay", value);
				            		break;
				            	case 9 : 
				            		long make_pay = (long) (Long.parseLong(rstMap.get("make_pay").toString())*0.1);
				            		rstMap.put("tax", make_pay);
				            		break;
				            	case 10 : 
				            		long ground_pay = (long) (Long.parseLong(rstMap.get("ground_pay").toString()));
				            		long make_pay2 = (long) (Long.parseLong(rstMap.get("make_pay").toString()));
				            		long tax = (long) (Long.parseLong(rstMap.get("tax").toString()));
				            		long sales_tax = ground_pay+make_pay2+tax;
				            		rstMap.put("sales_tax", sales_tax);
				            		break;
				            	case 25 : 
				            		rstMap.put("facility_use", value);
				            		break;
				            }
			        	}
				            
				    }
				        
			        
		        	if(!StringUtils.isEmpty(rstMap.get("numbers"))){
		        		if(!StringUtils.isEmpty(rstMap.get("sales_tax")) && !StringUtils.isEmpty(rstMap.get("tax"))){
				        	long sales_tax = (long) (Long.parseLong(rstMap.get("sales_tax").toString()));
				        	long tax = (long) (Long.parseLong(rstMap.get("tax").toString()));
				        	long sales = sales_tax-tax;
				        	rstMap.put("sales", sales);
				        }
				        
				        /**
				         * 평 단가구하기
				         */
				        if(!StringUtils.isEmpty(rstMap.get("sales"))){
				        	long sales = (long) (Long.parseLong(rstMap.get("sales").toString()));
				        	double contact_area_cal = Double.parseDouble(rstMap.get("contact_area_cal").toString());
				        	
				        	//평수구하기 계약면적*0.3025
				        	double ground_one_cul = contact_area_cal*0.3025;
				        	double ground_one = Math.round(ground_one_cul*100)/100.0; 
				        	rstMap.put("ground_one", ground_one);
				        	
				        	double exclusive_area = Double.parseDouble(rstMap.get("exclusive_area").toString());
				        	double ground_one_ex_cul = exclusive_area*0.3025;
				        	double ground_one_ex = Math.round(ground_one_ex_cul*100)/100.0; 
				        	rstMap.put("ground_one_ex", ground_one_ex);

				        	//평단가 구하기 분양가(세금미포함)/평수
				        	long rating = (long) (sales/ground_one_cul);
				        	rstMap.put("rating", rating);
				        }
				        
				        /*
				        System.out.println(""); 
				        System.out.println("호수 : "+rstMap.get("numbers"));
				        System.out.println("전용면적 : "+rstMap.get("exclusive_area"));
				        System.out.println("계약면적 : "+rstMap.get("contact_area"));
				        System.out.println("세금 : "+rstMap.get("tax"));
				        System.out.println("분양가(세금미포함) : "+rstMap.get("sales"));
				        System.out.println("분양가 : "+rstMap.get("sales_tax"));
				        System.out.println("평단가 : "+rstMap.get("rating"));
				        System.out.println("시설 : "+rstMap.get("facility_use"));
				        System.out.println("평수 : "+rstMap.get("ground_one"));
				        */
				        list.add(rstMap);
				          
	//			        System.out.print(" | ");
	//			        System.out.println("");
	//			        System.out.println("---------------------------------------------------------------------------------------------------------");
		        	}
			        
			    }
			}
		}
		return list;
	}
	
	
	/**
	 * 시트 2번에서 현장명을 가져온다.
	 */
	public String getSheetTwoData(String winFilePath) throws IOException {
		String store_name = "";
		
		FileInputStream fis=new FileInputStream(winFilePath);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		int rowindex=0;
		int columnindex=0;
		
		//시트 수 (첫번째에만 존재하므로 0을 준다)
		//만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
		XSSFSheet sheet=workbook.getSheetAt(1);
		
		//행의 수
		int rows=sheet.getPhysicalNumberOfRows();
		System.out.println("---------------------------------------------------------------------------------------------------------");
		for(rowindex=1;rowindex<rows;rowindex++){
			
			
		    //행을읽는다
		    XSSFRow row=sheet.getRow(rowindex);
		    if(row !=null){
		    	
		    	
		        //셀의 수
		        int cells=row.getPhysicalNumberOfCells();
		        String before = "";
		        for(columnindex=0;columnindex<=cells;columnindex++){
		        	
		            //셀값을 읽는다
		            XSSFCell cell=row.getCell(columnindex);
		            String value="";
		            //셀이 빈값일경우를 위한 널체크
		            if(cell==null){
		                continue;
		            }else{
		                //타입별로 내용 읽기
		                switch (cell.getCellType()){
		                case XSSFCell.CELL_TYPE_FORMULA:
		                    value=cell.getCellFormula();
		                    break;
		                case XSSFCell.CELL_TYPE_NUMERIC:
		                    value=cell.getNumericCellValue()+"";
		                    break;
		                case XSSFCell.CELL_TYPE_STRING:
		                    value=cell.getStringCellValue()+"";
		                    break;
		                case XSSFCell.CELL_TYPE_BLANK:
		                    value=cell.getBooleanCellValue()+"";
		                    break;
		                case XSSFCell.CELL_TYPE_ERROR:
		                    value=cell.getErrorCellValue()+"";
		                    break;
		                }
		            }
		            
		            
		            if(!Util.chkNull(before) && Util.chkNullData(before.replaceAll(" ", ""), "사업지")) {
		            	store_name = value;
		            }
		            
		            System.out.print(" | ");
		            System.out.print(value);
		            before = value;
		        }
		        System.out.print(" | ");
		        System.out.println("");
		        System.out.println("---------------------------------------------------------------------------------------------------------");
		    }
		}
		return store_name;
	}
}
