package com.kb.api.service.board;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.kb.api.persistence.MasterDao;
import com.kb.api.util.Common;
import com.kb.api.util.Util;


@Service
public class BoardService {
	@Inject MasterDao MasterDao;
	Util util = new Util();;
	
	
	public Map<String, Object> board_add(HashMap paramMap, Map<String, Object> map) throws Exception{
		
		map = this.getKakaoLocation(paramMap, map);
		if(Boolean.valueOf(map.get("kakao_success").toString())) {
			
			paramMap.put("x", map.get("x"));
			paramMap.put("y", map.get("y"));
			/**
			 * 주소 검색 성공
			 */
			int record = MasterDao.dataDelete("mapper.BoardMapper", "board_add", paramMap);
			if(record > 0) {
				map.put("success", true);
			}
		}
		Map<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("success", map.get("success"));
		
		if(!StringUtils.isEmpty(map.get("errorMsg"))) {
			rstMap.put("errorMsg", map.get("errorMsg"));
		}
		return rstMap;
	}
	public Map<String, Object> board_sel_add(HashMap paramMap, Map<String, Object> map) throws Exception{
		
		map = this.getKakaoLocation(paramMap, map);
		if(Boolean.valueOf(map.get("kakao_success").toString())) {
			
			paramMap.put("x", map.get("x"));
			paramMap.put("y", map.get("y"));
			/**
			 * 주소 검색 성공
			 */
			int record = MasterDao.dataDelete("mapper.BoardMapper", "board_add", paramMap);
			if(record > 0) {
				map.put("success", true);
			}
		}
		Map<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("success", map.get("success"));
		
		if(!StringUtils.isEmpty(map.get("errorMsg"))) {
			rstMap.put("errorMsg", map.get("errorMsg"));
		}
		return rstMap;
	}
	public Map<String, Object> board_bro_add(HashMap paramMap, Map<String, Object> map) throws Exception{
		
		map = this.getKakaoLocation(paramMap, map);
		if(Boolean.valueOf(map.get("kakao_success").toString())) {
			
			paramMap.put("x", map.get("x"));
			paramMap.put("y", map.get("y"));
			/**
			 * 주소 검색 성공
			 */
			int record = MasterDao.dataDelete("mapper.BoardMapper", "board_add", paramMap);
			
			if(record > 0) {
				MasterDao.dataCreate("mapper.BoardMapper", "board_bro_attach_add", paramMap); 
				map.put("success", true);
			} 
		}
		Map<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("success", map.get("success"));
		
		if(!StringUtils.isEmpty(map.get("errorMsg"))) {
			rstMap.put("errorMsg", map.get("errorMsg")); 
		}
		return rstMap;
	} 
	 
	
	
	public Map<String, Object> getKakaoLocation(HashMap paramMap, Map<String, Object> map) throws Exception {
		map.put("kakao_success", false);
		if(StringUtils.isEmpty(paramMap.get("addr"))) {  
			map.put("errorMsg", "필수값 누락(addr)");
			return map;
		}
		
		String url = "https://dapi.kakao.com/v2/local/search/address.json?query="+URLEncoder.encode(paramMap.get("addr").toString(), "UTF-8");
		String rst = util.sendRequest(url, "GET", "Authorization| KakaoAK 5bd16bc09a312f8111727c778b9efdaa", null);
		JSONParser parser = new JSONParser();
		JSONObject jObject = (JSONObject) parser.parse(rst); 
		
		
		if(jObject != null) {
			List<HashMap<String, Object>> documents = null;
			documents = (List<HashMap<String, Object>>) jObject.get("documents");
			
			if(documents == null) {
				map.put("errorMsg", "카카오 주소 API 연동중 에러가 발생하였습니다.");
			}
			
			if(documents.size() == 0) {
				map.put("errorMsg", "카카오 주소 API 연동중 에러가 발생하였습니다.(주소가 조회되지 않습니다.)");
			}
			
			if(documents != null && documents.size() > 1) {
				map.put("errorMsg", "카카오 주소 API 연동중 에러가 발생하였습니다.(주소가 여러개가 조회 됩니다. 상세한 주소를 입력해주세요.)");
			}
			
			if(documents != null && documents.size() == 1) {
				String x = (String) documents.get(0).get("x"); 
				String y = (String) documents.get(0).get("y");
				map.put("x", x);
				map.put("y", y);
				map.put("kakao_success", true);
			}
		}
		return map;
	}
	
	
	
	public Map<String, Object> notice_sue_add(HashMap paramMap, Map<String, Object> map){
		int before_sue_cnt = MasterDao.dataCount("mapper.BoardMapper", "read_sue_is", paramMap);
		
		/**
		 * 이전에 신고한 내역이 없다면
		 */
		if(before_sue_cnt <= 0) {
			int record = MasterDao.dataCreate("mapper.BoardMapper", "board_sue_add", paramMap);
			if(record > 0) {
				HashMap<String, Object> retBoardMap = null;
				retBoardMap = (HashMap<String, Object>) MasterDao.dataRead("mapper.BoardMapper", "read", paramMap);
				 
				if(retBoardMap != null) { 
					int sue_cnt = Integer.parseInt(retBoardMap.get("sue_cnt").toString());
					if(sue_cnt >= 3) {
						MasterDao.dataUpdate("mapper.BoardMapper", "board_update_show_yn", paramMap);
					}
					map.put("success", true);
				}
			}
		}else {
			map.put("errorMsg", "이전에 신고한 내역이 존재합니다.");
		}
		return map;
	}
	 
	 
	public Map<String, Object> comment_sue_add(HashMap paramMap, Map<String, Object> map){
		int before_sue_cnt = MasterDao.dataCount("mapper.BoardMapper", "read_sue_is_comment", paramMap);
		  
		/**
		 * 이전에 신고한 내역이 없다면
		 */
		if(before_sue_cnt <= 0) {
			int record = MasterDao.dataCreate("mapper.BoardMapper", "board_sue_add_comment", paramMap);
			if(record > 0) {
				HashMap<String, Object> retBoardMap = null;
				retBoardMap = (HashMap<String, Object>) MasterDao.dataRead("mapper.BoardMapper", "read_comment", paramMap);
				 
				if(retBoardMap != null) { 
					int sue_cnt = Integer.parseInt(retBoardMap.get("sue_cnt").toString());
					if(sue_cnt >= 3) {
						MasterDao.dataUpdate("mapper.BoardMapper", "board_update_show_yn_comment", retBoardMap);
					}
					map.put("success", true);
				}
			}
		}else {
			map.put("errorMsg", "이전에 신고한 내역이 존재합니다.");
		}
		return map;
	}
	
}
