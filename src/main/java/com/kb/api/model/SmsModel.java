package com.kb.api.model;

public class SmsModel {

	
//{"result_code":"1","message":"success","msg_id":"201918879","success_cnt":1,"error_cnt":0,"msg_type":"SMS"}
	String result_code;
	String message;
	String msg_id;
	int success_cnt;
	int error_cnt;
	String msg_type;
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMsg_id() {
		return msg_id;
	}
	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}
	public int getSuccess_cnt() {
		return success_cnt;
	}
	public void setSuccess_cnt(int success_cnt) {
		this.success_cnt = success_cnt;
	}
	public int getError_cnt() {
		return error_cnt;
	}
	public void setError_cnt(int error_cnt) {
		this.error_cnt = error_cnt;
	}
	public String getMsg_type() {
		return msg_type;
	}
	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}
	
	
}
